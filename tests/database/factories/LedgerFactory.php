<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Database\Factories;

use Altek\Accountant\Context;
use Altek\Accountant\Models\Ledger;
use Altek\Accountant\Notary;
use Altek\Accountant\Tests\Models\Article;
use Altek\Accountant\Tests\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class LedgerFactory extends Factory
{
    /**
     * {@inheritDoc}
     */
    protected $model = Ledger::class;

    /**
     * {@inheritDoc}
     */
    public function definition(): array
    {
        return [
            'user_id'         => UserFactory::new(),
            'user_type'       => User::class,
            'recordable_id'   => ArticleFactory::new(),
            'recordable_type' => Article::class,
            'context'         => Context::WEB,
            'event'           => 'updated',
            'properties'      => [],
            'modified'        => [],
            'extra'           => [],
            'url'             => $this->faker->url,
            'ip_address'      => $this->faker->ipv4,
            'user_agent'      => $this->faker->userAgent,
            'pivot'           => [],
            'signature'       => static function (array $properties) {
                unset($properties['signature']);

                return Notary::sign($properties);
            },
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ];
    }
}
